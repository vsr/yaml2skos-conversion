# About
This repository contains description of a custom YAML format to convert
taxonomies from an arbitrary format to SKOS.

## Usage

This script converts the custom YAML format (see below) to SKOS.

    python3 yaml2skos.py sample.yaml sample_skos.rdf

Options:

- `-h` shows a help message and exits.
- `--overwrite` specifies that any output file that already exists will be overwritten without a manual check.
- `--lang` specifies the default language to use for RDF Literals, defaults to `en`.
- `-f` specifies the output RDF serialization format. Defaults to 'xml'. Can be any format that `rdflib` can serialize to.

## YAML format

The documentation of the intermediate YAML format is located in
[YAML-format.md](YAML-format.md)

## License

This project is licensed under MIT license - see the [LICENSE](LICENSE) file for more information.
