# YAML format

This document describes the intermediate YAML format in detail. The format was created with the [SKOS](https://www.w3.org/2004/02/skos/) vocabulary in mind, familiarity with thesauri and the SKOS vocabulary is assumed.

## Commented Example

This is a fully commented example of all aspects of the YAML format:

    meta
    	uri: <uri> # default namespace
    	id: <id> # internal id, used for filenames
    	namespaces: # optional
    		- <name>: <uri> # additional namespaces
    	dcterms: # for metadata, optional
    		- <dct_concept>: <value> # <dct_concept> must be inside the dcterms namespace
    concepts: # list of concepts
    	<concept>: <prefLabel> # short definition of a concept
    	<concept>: # full definition of a concept
    		- l: <prefLabel>
    		- alt: <altLabel> # optional
    		- hidden: <hiddenLabel> # optional
    		- def: <definition> # optional
    		- note: <note> # optional
    		- scope: <scopeNote> # optional
    		- example: <example> # optional
    		- history: <historyNote> # optional
    		- editorial: <editorialNote> # optional
    		- change: <changeNote> # optional
    		- notation: <notation> # optional
    		- related: <concept> # reference to a concept, optional
    		- broader:
    			- <concept> # reference to a list of concepts, optional
    		- narrower:
    			- <concept> # referenced concept
    			- <concept>: <prefLabel> # nested shorthand definition of a concept
    			- <concept>: # nested full definition of a concept
    				- l: <prefLabel> # same format as above, but nested inside
    				- <detail>: <value>
    		- exactMatch: <concept> # optional
    		- closeMatch: <concept> # optional
    		- broadMatch: <concept> # optional
    		- narrowMatch: <concept> # optional
    		- relatedMatch: <concept> # optional
    collections:
    	<collection>:
    		- l: <prefLabel> # name of collection
    		- <concept>
    ordered-collections:
    	<identifier>:
    		- l: <prefLabel> # name of collection
    		- <concept>

## Structure

This is a minimal example of the structure:

    meta
    	uri: http://example.org/ex/
    	id: example_tax
    concepts:
    	thing: Thing

The format is structured into two main parts, identified by the `meta` and the `concepts` keys. The `meta` key contains key-value pairs for meta data. At the minimum, the `uri` and `id` must be specified. `uri` contains a URL and is used as the default namespace, that is all identifiers that have no namespace specified use this URL as the namespace. In this example, the concept `thing` would receive the identifier `http://example.org/ex/thing`. The `id` is used as the internal name for the taxonomy. The `concepts` key contains key-value pairs of concept definitions (see next section).

Beyond these two mandatory keys, two optional keys may be specified: `collections` and `ordered-collections`. Both of these keys are structured the same, and contain key-value pairs for SKOS collections and ordered-collections respectively. Each collection is specified by the identifier as a key and a list of values. If a value contains a key-value pair with key `l`, then this value is used for the preferred label of the collection. Otherwise, if no key-value pair but instead an identifier is specified, then this identifier will be added to the collection. It should be an identifier of a concept specified in the `concepts` part.

    meta
    	uri: http://example.org/ex/
    	id: example_tax
    concepts:
    	thing1: Thing 1
    	thing2: Thing 2
    	thing3: Thing 3
    	thing4: Thing 4
    collections:
    	even_things:
    		- l: Even Things
    		- thing2
    		- thing4
    ordered-collections:
    	little_things:
    		- l: Things 1-3
    		- thing1
    		- thing2
    		- thing3

## Concept definitions

Concepts can be defined directly in the `concepts` section, or within another concept (nested definition). Additionally, concepts can be defined in full, or use a shorthand definition.

The most basic case is the shorthand definition directly in the `concepts` section (last line):

    concepts:
    	thing: Thing

This defines a single concept with identifier `http://example.org/ex/thing` and skos:prefLabel `Thing`.

The second case is the full definition in the `concepts` section:

    concepts:
    	thing:
    		- l: Thing

This defines the exact same concept, that is both variants are equivalent. However, the full definition allows to define additional information:

    concepts:
    	thing:
    		- l: Thing                   # skos:prefLabel
    		- alt: Ding                  # skos:altLabel
    		- hidden: Ting               # skos:hiddenLabel
    		- def: Just an example thing # skos:definition
    		- note: This is an example   # skos:note
    		- scope: Only this document  # skos:scopeNote
    		- example: Anything          # skos:example
    		- history: Since today       # skos:historyNote
    		- editorial: By Lucas        # skos:editorialNote
    		- change: Unchanged          # skos:changeNote
    		- notation: Thing            # skos:notation

Beyond literals, relations between concepts can also be defined:

    concepts:
    	thing1:
    		- l: Main Thing
    		- narrower: thing2          # skos:narrower
    	thing2:
    		- l: Thing
    		- related: other_thing      # skos:related
    		- narrower: narrower_thing1 # skos:narrower
    		- narrower: narrower_thing2 # skos:narrower
    	narrower_thing1: Thing 3
    	narrower_thing2: Thing 4
    	other_thing: Other Thing

As can be seen in the previous example, SKOS relations can be added with the respective keyword and an identifier. At the same time, the example highlights two other important aspects: the reason that a list is used for the details of a concept, because it allows to define multiple kinds of the same relation (`skos:narrower` in this case), and secondly that shorthand and full definitions can be mixed.

For relations to other concepts, such as `skos:narrower` or `skos:related`, lists can also be used:

    concepts:
    	thing1:
    		- l: Main Thing
    		- narrower:
    			- narrower_thing1
    			- narrower_thing2
    	narrower_thing1: Narrower Thing 1
    	narrower_thing2: Narrower Thing 2

Another aspect that is only possible with full definitions, is to define nested concepts. Wherever lists are used for relations, as seen in the previous example, it is also possible to place a nested shorthand or full definition instead:

    concepts:
    	thing1:
    		- l: Main Thing
    		- narrower:
    			- narrower_thing1: Narrower Thing 1 # nested shorthand
    			- narrower_thing2:                  # nested full
    				- l: Narrower Thing 2

This example is equivalent to the previous one.

## Reference

### Top-Level Keys

These keys are allowed at the top-level of the structure.

#### meta

Contains meta information.

Allowed content: `uri`, `id`, `namespaces`, `dcterms`, (`title`)

##### uri

Required, default namespace.

Allowed content: URL

##### id

Required, used as internal reference and for filenames

Allowed content: string

##### namespaces

Optional, defines namespaces.

Allowed content: key-value pairs. Each key specifies the namespace shorthand, the value specifies the URL

Example:

    namespaces:
    	ex1: http://example.org/ex1/

Namespaces defined here can be used wherever identifiers are used:

    concepts:
    	thing:
    		- l: Thing
    		- exactMatch: ex1:Thing

Namespaces are implemented, but not thoroughly tested; expect issues.

##### dcterms

Optional, specifies meta information using the [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/).

Allowed content: a list of key-value pairs. Each key must be a valid name from the DCMI Metadata Terms vocabulary, the value mast be a valid literal for that name.

Example:

    dcterms:
    	- creator: Jane Doe
    	- creator: John Doe
    	- title: My Example Taxonomy

###### title

Optional, alternative way to specify the DCMI Metadata Terms title:

    title: My Example Taxonomy

#### concepts

Defines all concepts, either directly or nested.

Allowed content: collection definition. See further below.

#### collections

Defines unordered collections.

Allowed content: collection definition. See further below.

#### ordered-collections

Defines ordered collections.

Allowed content: collection definition. See further below.

### concept definitions

A concept can be specified in either shorthand or full definition.

Allowed content: string or list of key-value pairs. See shorthand definition or full definition.

#### shorthand definition

The shorthand definition only contains the identifier and the preferred label.

Allowed content: string

Example:

    concept_identifier: Concept Name

#### full definition

The full definition contains at least the preferred label, but may additionally contain further details. See further below for acceptable keys.

Allowed content: list of key-value pairs. See concept details

Example:

    concept_identifier:
    	- l: Concept Name
    	- def: Concept Definition
    	- related: other_concept_identifier

### collection definitions

A collection is defined by an identifier and a list of identifiers, which should also contain a preferred label similar to the full concept definition.

Example:

    collection_identifier:
    	- l: Collection Name
    	- concept_identifier_1
    	- concept_identifier_2
    	- concept_identifier_3

### concept details

The following keys are allowed as concept details:

#### l

Required, preferred label: `skos:prefLabel`

Allowed content: String

#### alt

Optional, alternative label: `skos:altLabel`

Allowed content: String

#### hidden

Optional, hidden label: `skos:hiddenLabel`

Allowed content: String

#### def

Optional, concept definition: `skos:definition`

Allowed content: String

#### note

Optional, general note: `skos:note`

Allowed content: String

#### scope

Optional, scope note: `skos:scopeNote`

Allowed content: String

#### example

Optional, example of concept: `skos:example`

Allowed content: String

#### history

Optional, history note: `skos:historyNote`

Allowed content: String

#### editorial

Optional, editorial note: `skos:editorialNote`

Allowed content: String

#### change

Optional, change note: `skos:changeNote`

Allowed content: String

#### notation

Optional, notation: `skos:notation`

Allowed content: String

#### related

Optional, related concept in the same thesaurus: `skos:related`

Allowed content: concept identifier or list of concept definitions

#### narrower

Optional, narrower concept in the same thesaurus: `skos:narrower`

Allowed content: concept identifier or list of concept definitions

#### broader

Optional, broader concept in the same thesaurus: `skos:broader`

Allowed content: concept identifier or list of concept definitions

#### exactMatch

Optional, exactly matching concept in a different thesaurus: `skos:exactMatch`

Allowed content: concept identifier or list of concept definitions

#### closeMatch

Optional, closely matching concept in a different thesaurus: `skos:closeMatch`

Allowed content: concept identifier or list of concept definitions

#### broadMatch

Optional, broader matching concept in a different thesaurus: `skos:broadMatch`

Allowed content: concept identifier or list of concept definitions

#### narrowMatch

Optional, narrower matching concept in a different thesaurus: `skos:narrowMatch`

Allowed content: concept identifier or list of concept definitions

#### relatedMatch

Optional, related matching concept in a different thesaurus: `skos:relatedMatch`

Allowed content: concept identifier or list of concept definitions
